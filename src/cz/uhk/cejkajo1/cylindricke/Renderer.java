package cz.uhk.cejkajo1.cylindricke;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import transforms.Camera;
import transforms.Mat4;
import transforms.Mat4PerspRH;
import transforms.Vec3D;
import utils.OGLBuffers;
import utils.ShaderUtils;
import utils.ToFloatArray;

import java.awt.event.*;

/**
 * Definice těles pomocí parametrických funkcí - cylindrické souřadnice
 * @author cejkajo1
 */

public class Renderer implements GLEventListener, MouseListener,
        MouseMotionListener, KeyListener {

    int width, height, ox, oy;

    OGLBuffers grid;

    int shaderGrid, locMatGrid, locLightGrid;

    Camera cam = new Camera();
    Mat4 proj;
    Vec3D lightPos = new Vec3D(4, 2, 1);

    public void init(GLAutoDrawable glDrawable) {
        GL2 gl = glDrawable.getGL().getGL2();

        System.out.println("Init GL is " + gl.getClass().getName());
        System.out.println("OpenGL version " + gl.glGetString(GL2.GL_VERSION));
        System.out.println("OpenGL vendor " + gl.glGetString(GL2.GL_VENDOR));
        System.out.println("OpenGL renderer " + gl.glGetString(GL2.GL_RENDERER));
        System.out.println("OpenGL extensions "
                + gl.glGetString(GL2.GL_EXTENSIONS));

        shaderGrid = ShaderUtils.loadProgram(gl, "./shader/cylindricke/grid");

        grid = GeometryGenerator.generateGrid(gl, "inPosition", 100, 100);

        locMatGrid = gl.glGetUniformLocation(shaderGrid, "mat");
        locLightGrid = gl.glGetUniformLocation(shaderGrid, "lightPos");

        cam.setPosition(new Vec3D(5, 5, 2.5));
        cam.setAzimuth(Math.PI * 1.25);
        cam.setZenith(Math.PI * -0.125);

        gl.glEnable(GL2.GL_DEPTH_TEST);
    }

    public void display(GLAutoDrawable glDrawable) {
        GL2 gl = glDrawable.getGL().getGL2();

        gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

        //grid
        gl.glUseProgram(shaderGrid);
        gl.glUniformMatrix4fv(locMatGrid, 1, false,
                ToFloatArray.convert(cam.getViewMatrix().mul(proj)), 0);
        gl.glUniform3fv(locLightGrid, 1, ToFloatArray.convert(lightPos), 0); // světlo
        grid.draw(GL2.GL_TRIANGLES, shaderGrid);
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width,
                        int height) {
        this.width = width;
        this.height = height;
        proj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 0.01, 1000.0);
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
                               boolean deviceChanged) {
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
        ox = e.getX();
        oy = e.getY();
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
        cam.addAzimuth(Math.PI * (ox - e.getX())
                / width);
        cam.addZenith(Math.PI * (e.getY() - oy)
                / width);
        ox = e.getX();
        oy = e.getY();
    }

    public void mouseMoved(MouseEvent e) {
    }

    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_W:
                cam.forward(1);
                break;
            case KeyEvent.VK_D:
                cam.right(1);
                break;
            case KeyEvent.VK_S:
                cam.backward(1);
                break;
            case KeyEvent.VK_A:
                cam.left(1);
                break;
            case KeyEvent.VK_SHIFT:
                cam.down(1);
                break;
            case KeyEvent.VK_CONTROL:
                cam.up(1);
                break;
            case KeyEvent.VK_SPACE:
                cam.setFirstPerson(!cam.getFirstPerson());
                break;
            case KeyEvent.VK_R:
                cam.mulRadius(0.9f);
                break;
            case KeyEvent.VK_F:
                cam.mulRadius(1.1f);
                break;
        }
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyTyped(KeyEvent e) {
    }

    public void dispose(GLAutoDrawable arg0) {
    }
}