#version 330
const vec4 ambient = vec4(0.1, 0.0, 0.0, 1.0);
const vec4 color = vec4(1.0, 1.0, 0.5, 1.0);

uniform sampler2D textureUnit;
uniform sampler2D normalTextureUnit;

in vec3 lightDirection;
in vec3 eyeDirection;
in vec2 texCoord;
in mat3 normalMapMatrix;

float diffuse = 0.0;
vec3 reflection;
float specular = 0.0;

vec4 textureColor;
vec4 normalColor;

vec3 normal;
vec3 toLight;

vec2 cBumpSize;
float height;

void main() {

    textureColor = texture2D(textureUnit,texCoord,0.0);

    //parallax mapping
    cBumpSize = vec2(0.04, -0.02);
    height = (texture2D(normalTextureUnit,texCoord,0.0).z-0.5f) * cBumpSize.x + cBumpSize.y;
    normalColor = texture2D(normalTextureUnit, texCoord + eyeDirection.xy * height, 0.0);

    normal = vec3(2.0*(normalColor - 0.5));

    toLight = normalize(lightDirection);

    diffuse = dot(toLight,normal);

    diffuse = clamp(diffuse,0.0,1.0);

	vec2 cBumpSize = vec2(0.02, -0.01);
	float height = cos(texCoord.x * PI * 20)/2 * cBumpSize.x + cBumpSize.y;
    normal = height * normal * toLight;
    specular = 0.0;

    if(diffuse > 0.0){
        reflection = normalize(reflect(-toLight,normal));

        specular = dot(reflection,normalize(eyeDirection));

        if(specular > 0.0) {
            specular = 0.1*pow(specular,40.0);
         } else {
            specular = 0.0;
        }
    }

    gl_FragColor =  textureColor * clamp(diffuse*color + ambient,0.0,1.0)
                        + vec4(specular,specular,specular,0.0);
}
