#version 330
in vec2 inPosition;
uniform mat4 mat;
uniform vec3 lightPos;
float PI = 3.1415927;

out vec3 eyeDirection;
out vec3 lightDirection;
out vec2 texCoord;

vec3 normalBlue;
vec3 normalRed;
vec3 normalGreen;
out mat3 normalMapMatrix;

vec3 paramSurf(vec2 uv) {
	float a = 2 * PI * uv.x; // azimut
	float t = PI * (0.5 - uv.y); // zenit
	return vec3(
		cos(a) * cos(t),
		sin(a) * cos(t),
		sin(t)
	);
}

void main() {
	vec3 position = paramSurf(inPosition);
	vec3 tux = paramSurf(inPosition + vec2(0.001, 0)) - position;
	vec3 tvy = paramSurf(inPosition + vec2(0, 0.001)) - position;

	normalBlue = normalize(cross(tvy, tux));
    normalRed = normalize(vec3(-normalBlue.y,normalBlue.x,0.0));
    normalGreen = cross(normalBlue,normalRed);

    normalMapMatrix = mat3(normalRed.x,normalGreen.x,normalBlue.x,
    normalRed.y,normalGreen.y,normalBlue.y,
    normalRed.z,normalGreen.z,normalBlue.z);

    lightDirection = normalize(lightPos - position);
    lightDirection = normalMapMatrix * lightDirection;

    eyeDirection = normalize(-position);
    eyeDirection = normalMapMatrix * eyeDirection;

	gl_Position = mat * vec4(position, 1.0);
	texCoord = inPosition;
} 
