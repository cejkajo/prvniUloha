#version 330

in vec2 inPosition;
out vec4 color;
uniform mat4 mat;
uniform vec3 lightPos;

float PI = 3.1415927;
const vec4 ambient = vec4(0.1, 0.0, 0.0, 1.0);
const vec4 diffuse = vec4(0.5, 0.0, 0.0, 1.0);
const vec4 specular = vec4(1.0, 1.0, 1.0, 1.0);

vec3 paramSurf(vec2 uv) {
 	float s = 2 * PI * uv.x; // azimuth
	float t = PI * uv.y; // zenit

	// elipsoid
	return vec3(
		sin(t)*cos(s),
		2*sin(t)*sin(s),
		cos(t)
	);
}

void main () {

	vec4 spec = vec4(0.0);

    vec3 position = paramSurf(inPosition);
    vec3 tu = paramSurf(inPosition + vec2(0.001, 0)) - position;
    vec3 tv = paramSurf(inPosition + vec2(0, 0.001)) - position;

	vec3 n = normalize(cross(tv, tu));
    // blinn phong
	float intensity = max(dot(n, lightPos), 0.0);
	if (intensity > 0.0) {
		vec3 eye = normalize(-position);
		vec3 h = normalize(lightPos + eye);
		float intSpec = max(dot(h,n), 0.0);
		spec = specular * pow(intSpec, 16);
	}
	color = max(intensity * diffuse + spec, ambient);
	gl_Position = mat * vec4(position, 1.0);
}
