#version 330
in vec2 inPosition;
out vec3 vertColor;
uniform mat4 mat;
uniform vec3 lightPos;
float PI = 3.1415927;

// spocita azimut a zenit ze souradnic
vec3 paramSurf(vec2 uv) {
	float t = PI * uv.y; // zenit
	float s = 2 * PI * uv.x; // azimuth
	
	//alien
	//float r = 2+cos(2*t)*sin(2*s);
	
	//autorska funkce
	float r = 1+0.2*sin(6*s)*sin(5*t)*cos(s);
	
	return vec3(
		    r*sin(t)*cos(s),
		    r*sin(t)*sin(s),
			r*cos(t)
		);
}

void main() {
	vec3 position = paramSurf(inPosition);
	vec3 tux = paramSurf(inPosition + vec2(0.001, 0)) - position;
	vec3 tvy = paramSurf(inPosition + vec2(0, 0.001)) - position;
	vec3 n = normalize(cross(tvy, tux));
	vec3 lightVec = normalize(lightPos - position) ;
	float diffuse = max(dot(n, lightVec), 0.0);
	gl_Position = mat * vec4(position, 1.0);
	vertColor = vec3(diffuse);
}
