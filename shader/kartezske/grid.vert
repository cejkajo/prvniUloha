#version 330
in vec2 inPosition;
out vec3 vertColor;
uniform mat4 mat;
uniform vec3 lightPos;

float PI = 3.1415927;

// spocita azimut a zenit ze souradnic
vec3 paramSurf(vec2 uv) {
 	float s = 2 * PI * uv.x; // azimuth
	float t = PI * uv.y; // zenit

    //autorska funkce
    return vec3(
        sin(t)*cos(s)+cos(s),
        sin(t)*sin(s)+sin(s),
        cos(t)
    );
/**
	// elipsoid
	return vec3(
		sin(t)*cos(s),
		2*sin(t)*sin(s),
		cos(t)
	);
**/
}

void main() {
	vec3 position = paramSurf(inPosition);
	vec3 tux = paramSurf(inPosition + vec2(0.001, 0)) - position;
	vec3 tvy = paramSurf(inPosition + vec2(0, 0.001)) - position;
	vec3 n = normalize(cross(tvy, tux));
	vec3 lightVec = normalize(lightPos - position) ;
	float diffuse = max(dot(n, lightVec), 0.0);
	gl_Position = mat * vec4(position, 1.0);
	vertColor = vec3(diffuse);
}
