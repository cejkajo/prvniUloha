#version 330

in vec2 inPosition;
out vec3 normal;
out vec3 eye;
uniform mat4 mat;
uniform vec3 lightPos;

float PI = 3.1415927;

vec3 paramSurf(vec2 uv) {
 	float s = 2 * PI * uv.x; // azimuth
	float t = PI * uv.y; // zenit

	// elipsoid
	return vec3(
		sin(t)*cos(s),
		2*sin(t)*sin(s),
		cos(t)
	);
}

void main () {

    vec3 position = paramSurf(inPosition);
    vec3 tu = paramSurf(inPosition + vec2(0.001, 0)) - position;
    vec3 tv = paramSurf(inPosition + vec2(0, 0.001)) - position;

	normal = normalize(cross(tv, tu));
    eye = -position;

	gl_Position = mat * vec4(position, 1.0);
}
