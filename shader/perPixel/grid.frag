#version 330

in vec3 normal;
in vec3 eye;

out vec4 colorOut;
uniform vec3 lightPos;

const vec4 ambient = vec4(0.1, 0.0, 0.0, 1.0);
const vec4 diffuse = vec4(0.5, 0.0, 0.0, 1.0);
const vec4 specular = vec4(1.0, 1.0, 1.0, 1.0);

void main() {
    // blinn phong
    vec4 spec = vec4(0.0);

    vec3 n = normalize(normal);
    vec3 e = normalize(eye);

    float intensity = max(dot(n,lightPos), 0.0);

    if (intensity > 0.0) {
        vec3 h = normalize(lightPos + e);
        float intSpec = max(dot(h,n), 0.0);
        spec = specular * pow(intSpec,16);
    }
    colorOut = max(intensity *  diffuse + spec, ambient);
}