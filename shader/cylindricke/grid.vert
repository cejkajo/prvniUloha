#version 330
in vec2 inPosition;
out vec3 vertColor;
uniform mat4 mat;
uniform vec3 lightPos;
float PI = 3.1415927;

//spocita azimut a zenit ze souradnic
vec3 paramSurf(vec2 uv) {
/**	
 * 	sombrero
 	float s = 2 * PI * uv.x; // azimuth
	float t = 2 * PI * uv.y; // zenit
	float z = 2 * sin(t);
	float r = 0.5*t;
**/

	//juicer
 	float s = 2 * PI * uv.x; // azimuth
	float t = 2 * PI * uv.y; // zenit
	float z = cos(t);
	float r = t;

/**
	// muj disk
 	float s = 2 * PI * uv.x; // azimuth
	float t = 2 * PI * uv.y; // zenit
	float z = cos(t)*cos(t)*sin(t)*sin(t)*5;
	float r = t - 20;
**/
	return vec3(
		    r*cos(s),
		    r*sin(s),
			z
		);
}

void main() {
	vec3 position = paramSurf(inPosition);
	vec3 tux = paramSurf(inPosition + vec2(0.001, 0)) - position;
	vec3 tvy = paramSurf(inPosition + vec2(0, 0.001)) - position;
	vec3 n = normalize(cross(tvy, tux));
	vec3 lightVec = normalize(lightPos - position) ;
	float diffuse = max(dot(n, lightVec), 0.0);
	gl_Position = mat * vec4(position, 1.0);
	vertColor = vec3(diffuse);
}
