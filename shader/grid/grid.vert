#version 330
in vec2 inPosition;
out vec3 vertColor;
uniform mat4 mat;

float PI = 3.1415927;

void main() {
	vec3 position = vec3(inPosition, 0.0);
	vertColor = position;
    gl_Position = mat * vec4(position, 1.0);
}
