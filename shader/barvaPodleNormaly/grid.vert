#version 330
in vec2 inPosition;
out vec3 vertColor;
uniform mat4 mat;

float PI = 3.1415927;

// spocita azimut a zenit ze souradnic
vec3 paramSurf(vec2 uv) {
 	float s = 2 * PI * uv.x; // azimuth
	float t = PI * uv.y; // zenit

	// elipsoid
	return vec3(
		sin(t)*cos(s),
		2*sin(t)*sin(s),
		cos(t)
	);
}

void main() {
	vec3 position = paramSurf(inPosition);
	vec3 tux = paramSurf(inPosition + vec2(0.001, 0)) - position;
	vec3 tvy = paramSurf(inPosition + vec2(0, 0.001)) - position;
	vec3 n = normalize(cross(tvy, tux));
	vertColor = position  * n; //barva v závislosti na normále
    gl_Position = mat * vec4(position, 1.0);
}
